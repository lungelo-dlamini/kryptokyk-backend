const {ApolloServer, gql} = require("apollo-server");
const fetch = require("node-fetch");
const axios = require("axios").default

const typeDefs= gql`
type Query{
    getCoin(id: String!): Coin
    getCoins: [Coin!]!
  }
type Coin{
    id: String
    symbol: String
    name: String
    nameid: String
    rank: Int
    price_usd: String
  }
  `
;
const baseURL = 'https://api.coinlore.net/api/ticker/?id='


const resolvers = {
  Query:{
      //arguments in graphql resolver - parent,args,context,info
    getCoin: async(parent,{id},context)=>{
        try {
            const response = await axios.get(baseURL+id)
            console.log(response)
            // destructure array
            const [data] = response.data.data
      return data;
        } catch (error) {
            console.log(error)
            return error
        }
      
    },
    getCoins: async(parent,args,context,info)=>{
        try {
            const response = await axios.get('https://api.coinlore.net/api/tickers/')
            return response.data.data;
        } catch (error) {
            console.log(error)
            return error;
        }
    }
  }
};

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
